/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --disable-multilib --disable-werror --enable-languages=c --target=aarch64-linux-gnu --prefix=/home/rev3nt3ch/B14CKEN3D-TC/build-tools-gcc/aarch64-linux-gnu";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { NULL, NULL} };
